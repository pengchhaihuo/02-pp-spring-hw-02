package com.example.contentmgt.service;

import com.example.contentmgt.model.Content;

import java.util.List;

public interface ContentService {
    List<Content> getAll();
}

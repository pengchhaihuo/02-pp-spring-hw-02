package com.example.contentmgt.service;

import com.example.contentmgt.model.Content;
import com.example.contentmgt.repository.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ContentServiceImp implements ContentService{

    private ContentRepository contentRepository;
    @Autowired
    public ContentServiceImp(ContentRepository contentRepository) {
        this.contentRepository = contentRepository;
    }

    @Override
    public List<Content> getAll() {
        return contentRepository.getAllContent();
    }
}

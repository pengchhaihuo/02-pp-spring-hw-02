package com.example.contentmgt.controller;

import com.example.contentmgt.model.Content;
import com.example.contentmgt.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ContentController {

    private ContentService contentService;

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Autowired


    @GetMapping("/")
    public String viewIndex(ModelMap modelMap){
        modelMap.addAttribute("list",contentService.getAll());
        return "index";
    }
}

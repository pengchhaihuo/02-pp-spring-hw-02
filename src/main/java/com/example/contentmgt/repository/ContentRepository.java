package com.example.contentmgt.repository;

import com.example.contentmgt.model.Content;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ContentRepository {

    List<Content> list = new ArrayList<>();

    public ContentRepository() {
        for (int i = 1; i < 4; i++) {
            Content content = new Content();
            content.setId(i);
            content.setTitle("Programming");
            content.setDescription("Hello world");
            content.setImage("image");
            list.add(content);
        }
    }

    public List<Content> getAllContent(){
        return list;
    }

}

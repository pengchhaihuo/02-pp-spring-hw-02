package com.example.contentmgt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContentMgtApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContentMgtApplication.class, args);
    }

}
